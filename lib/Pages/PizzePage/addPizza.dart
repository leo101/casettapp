import 'package:casettapp/Models/pizza.dart';
import 'package:casettapp/Notifiers/pizzeNotifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

TextEditingController _nome = new TextEditingController();
TextEditingController _prezzo = new TextEditingController();

class AddPizza extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Aggiungi una pizza'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Material(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(8.0),
              child: TextField(
                controller: _nome,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                  hintText: 'Nome della pizza',
                  border: InputBorder.none,
                  filled: true,
                  fillColor: Colors.blue[800],
                  hintStyle: TextStyle(
                    color: Colors.grey[300],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            Material(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(8.0),
              child: TextField(
                controller: _prezzo,
                keyboardType: TextInputType.number,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                  hintText: 'Costo della pizza',
                  border: InputBorder.none,
                  filled: true,
                  fillColor: Colors.blue[800],
                  hintStyle: TextStyle(
                    color: Colors.grey[300],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 8.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  child: Text('Aggiungi'),
                  onPressed: () {
                    if (_prezzo.text.trim() != null &&
                        _prezzo.text.trim() != '' &&
                        _nome.text.trim() != null &&
                        _nome.text.trim() != '') {
                      Provider.of<PizzeNotifier>(context).addPizzaOnline(
                        new Pizza(
                          cost: double.parse(_prezzo.text),
                          name: _nome.text,
                        ),
                      );
                      Navigator.pop(context);
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
