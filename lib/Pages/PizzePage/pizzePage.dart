import 'package:casettapp/Models/ordine.dart';

import 'package:casettapp/Notifiers/pizzeNotifier.dart';
import 'package:casettapp/Pages/PizzePage/addPizza.dart';

import 'package:casettapp/Pages/SkeletonPage/skeletonpage.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

PizzeNotifier notifier;

bool payed = false;

class PizzePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    notifier = Provider.of<PizzeNotifier>(context);
    return Scaffold(
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text('Totale: ${notifier.prenotazione.getTotPrenotation()} €'),
          SizedBox(
            height: 24.0,
          ),
        ],
      ),
      body: notifier.prenotazione == null ||
              notifier.prenotazione.lista.length == 0
          ? Center(
              child: Text(
                'Nessuna prenotazione inserita',
                style: TextStyle(color: Colors.grey),
              ),
            )
          : ListView.builder(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              itemCount: notifier.prenotazione.lista.length > 0
                  ? notifier.prenotazione.lista.length + 1
                  : notifier.prenotazione.lista.length,
              itemBuilder: (context, index) => index ==
                      notifier.prenotazione.lista.length
                  ? Container(
                      height: 52,
                      margin: EdgeInsets.only(top: 16.0),
                      child: RaisedButton(
                        child: Text('Elimina gli ordini'),
                        textColor: Colors.white,
                        color: Colors.blue[800],
                        onPressed: () {
                          notifier.delPrenotazioni();
                        },
                      ),
                    )
                  : Draggable(
                      axis: Axis.horizontal,
                      childWhenDragging: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Material(
                            clipBehavior: Clip.antiAlias,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              bottomLeft: Radius.circular(8.0),
                            ),
                            child: Container(
                              color: Colors.blue[800],
                              width: MediaQuery.of(context).size.width / 2 - 8,
                              height: 64.0,
                              child: DragTarget(
                                onAccept: (_) {
                                  notifier.setPayedOrNot(index, false);
                                },
                                key: Key('NotPayed'),
                                builder: (context, _, __) {
                                  return Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      SizedBox(
                                        width: 16.0,
                                      ),
                                      Text(
                                        'Pagato',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ],
                                  );
                                },
                              ),
                            ),
                          ),
                          Material(
                            clipBehavior: Clip.antiAlias,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(8.0),
                              bottomRight: Radius.circular(8.0),
                            ),
                            child: Container(
                              color: Colors.red,
                              width: MediaQuery.of(context).size.width / 2 - 8,
                              height: 64.0,
                              child: DragTarget(
                                key: Key('Payed'),
                                onAccept: (_) {
                                  notifier.setPayedOrNot(index, true);
                                },
                                builder: (context, _, __) {
                                  return Center(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Text(
                                          'Non pagato',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        SizedBox(
                                          width: 16.0,
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      feedback: Material(
                        clipBehavior: Clip.antiAlias,
                        borderRadius: BorderRadius.circular(8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width - 16.0,
                          child: ListTile(
                            title: Text(
                                '${notifier.prenotazione.lista[index].nome}'),
                            trailing: Text(
                              '${notifier.prenotazione.getTot(index)} €',
                              style: TextStyle(
                                  color:
                                      notifier.prenotazione.lista[index].payed
                                          ? Colors.green
                                          : Colors.red),
                            ),
                            subtitle: Text(
                                '${notifier.prenotazione.getPizzeNames(index)}'),
                          ),
                        ),
                      ),
                      maxSimultaneousDrags: 1,
                      key: UniqueKey(),
                      child: ListTile(
                        title:
                            Text('${notifier.prenotazione.lista[index].nome}'),
                        trailing: Text(
                          '${notifier.prenotazione.getTot(index)} €',
                          style: TextStyle(
                              color: notifier.prenotazione.lista[index].payed
                                  ? Colors.green
                                  : Colors.red),
                        ),
                        subtitle: Text(
                            '${notifier.prenotazione.getPizzeNames(index)}'),
                      ),
                    ),
            ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(Icons.add),
        label: Text('Aggiungi ordine'),
        onPressed: () {
          showModalBottomSheet(
              context: context, builder: (context) => _AddOrdine());
        },
      ),
    );
  }
}

class _AddOrdine extends StatefulWidget {
  @override
  _AddOrdineState createState() => _AddOrdineState();
}

class _AddOrdineState extends State<_AddOrdine> {
  int personaSelected = 0;

  List<int> pizzeSelected = [0];
  List<Widget> pizzeDropDown = [];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Inserisci un nuovo ordine',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22.0),
          ),
          SizedBox(
            height: 32.0,
          ),
          Text('Seleziona una persona'),
          SizedBox(
            height: 8.0,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              DropdownButton(
                underline: Container(),
                onChanged: (index) {
                  setState(() {
                    personaSelected = index;
                  });
                },
                value: personaSelected,
                items: List.generate(mainNotifier.user.friends.length, (index) {
                  return DropdownMenuItem(
                    child: Text(mainNotifier.user.friends[index].username),
                    value: index,
                  );
                }),
              ),
              SizedBox(
                width: 32.0,
              ),
              Container(
                constraints: BoxConstraints(maxHeight: 256.0 - 59.0),
                width: 170.0,
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: pizzeSelected.length,
                  itemBuilder: (context, numero) {
                    return Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        DropdownButton(
                          value: pizzeSelected[numero],
                          onChanged: (index) {
                            setState(() {
                              pizzeSelected[numero] = index;
                            });
                          },
                          items: List.generate(
                            notifier.pizze.length,
                            (index) => DropdownMenuItem(
                              child: Text(notifier.pizze[index].name),
                              value: index,
                            ),
                          ),
                        ),
                        numero == 0
                            ? IconButton(
                                icon: Icon(
                                  Icons.add,
                                  color: Colors.grey,
                                ),
                                onPressed: () {
                                  setState(() {
                                    pizzeSelected.add(0);
                                  });
                                },
                              )
                            : IconButton(
                                icon: Icon(
                                  Icons.remove,
                                  color: Colors.grey,
                                ),
                                onPressed: () {
                                  setState(() {
                                    pizzeSelected.removeAt(numero);
                                  });
                                },
                              ),
                      ],
                    );
                  },
                ),
              )
            ],
          ),
          SwitchListTile(
            onChanged: (value) {
              setState(() {
                payed = value;
              });
            },
            value: payed,
            title: Text('Pagato'),
            subtitle: Text('Ordine già pagato'),
          ),
          Expanded(
            child: Container(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text('Aggiungi una pizza'),
                onPressed: () => Navigator.push(context,
                    new MaterialPageRoute(builder: (context) => AddPizza())),
              ),
              OutlineButton(
                textColor: Colors.blue[800],
                borderSide: BorderSide(color: Colors.blue[800]),
                child: Text('Aggiungi'),
                onPressed: () {
                  notifier.addOrdine(new Ordine(
                      nome: mainNotifier.user.friends[personaSelected].username,
                      payed: payed,
                      pizza: List.generate(pizzeSelected.length, (index) {
                        return notifier.pizze[pizzeSelected[index]];
                      })));
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
