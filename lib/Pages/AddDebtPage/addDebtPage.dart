import 'package:casettapp/Models/debt.dart';
import 'package:casettapp/Notifiers/mainNotifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

MainNotifier notifier;
TextEditingController controllerValue;
TextEditingController controllerTitle;

class AddDebtPage extends StatefulWidget {
  final int personIndex;

  AddDebtPage({
    this.personIndex,
  });

  @override
  _AddDebtPageState createState() => _AddDebtPageState();
}

class _AddDebtPageState extends State<AddDebtPage> {
  @override
  void initState() {
    controllerTitle = new TextEditingController();
    controllerValue = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (notifier == null) notifier = Provider.of<MainNotifier>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Aggiungi un debito'),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Material(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(4.0),
              child: TextField(
                controller: controllerValue,
                autofocus: true,
                style: TextStyle(color: Colors.white),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Valore del debito',
                  fillColor: Colors.blue[800],
                  filled: true,
                  hintStyle: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            Material(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(4.0),
              child: TextField(
                textCapitalization: TextCapitalization.sentences,
                controller: controllerTitle,
                autofocus: true,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Titolo del debito',
                  fillColor: Colors.blue[800],
                  filled: true,
                  hintStyle: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 8.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                RaisedButton(
                  textColor: Colors.white,
                  color: Colors.blue,
                  child: Text('AGGIUNGI'),
                  onPressed: () {
                    controllerValue.text =
                        controllerValue.text.replaceAll(',', '.');
                    notifier.addDebt(
                        Debt(
                            title: controllerTitle.text,
                            value: double.parse(controllerValue.text),
                            emailPerson: notifier
                                .user.friends[widget.personIndex].email),
                        widget.personIndex);
                    Navigator.pop(context);
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
