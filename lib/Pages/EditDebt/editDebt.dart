import 'package:casettapp/Models/debt.dart';
import 'package:casettapp/Notifiers/mainNotifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

MainNotifier notifier;
TextEditingController controllerValue;
TextEditingController controllerTitle;

class EditDebtPage extends StatefulWidget {
  final int personIndex;
  final int debtIndex;

  EditDebtPage({
    this.personIndex,
    this.debtIndex,
  });

  @override
  _EditDebtPageState createState() => _EditDebtPageState();
}

class _EditDebtPageState extends State<EditDebtPage> {
  @override
  void initState() {
    controllerTitle = new TextEditingController(text: '');
    controllerValue = new TextEditingController(text: '');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (notifier == null) notifier = Provider.of<MainNotifier>(context);
    if (controllerTitle.text == '')
      controllerTitle.text = notifier
          .user.friends[widget.personIndex].debiti[widget.debtIndex].title;
    if (controllerValue.text == '')
      controllerValue.text = notifier
          .user.friends[widget.personIndex].debiti[widget.debtIndex].value
          .toString();
    return Scaffold(
      appBar: AppBar(
        title: Text('Modifica il debito'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Material(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(4.0),
              child: TextField(
                controller: controllerValue,
                autofocus: true,
                style: TextStyle(color: Colors.white),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  prefixIcon: Icon(
                    Icons.euro_symbol,
                    color: Colors.white70,
                  ),
                  hintText: 'Valore del debito',
                  fillColor: Colors.blue[800],
                  filled: true,
                  hintStyle: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            Material(
              clipBehavior: Clip.antiAlias,
              borderRadius: BorderRadius.circular(4.0),
              child: TextField(
                textCapitalization: TextCapitalization.sentences,
                controller: controllerTitle,
                autofocus: true,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Titolo del debito',
                  fillColor: Colors.blue[800],
                  prefixIcon: Icon(
                    Icons.text_fields,
                    color: Colors.white70,
                  ),
                  filled: true,
                  hintStyle: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 8.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                RaisedButton(
                    textColor: Colors.white,
                    color: Colors.blue,
                    child: Text('MODIFICA'),
                    onPressed: () {
                      notifier.modDebt(
                        widget.personIndex,
                        widget.debtIndex,
                        new Debt(
                          title: controllerTitle.text,
                          value: double.parse(controllerValue.text),
                        ),
                      );
                      Navigator.pop(context);
                    }),
              ],
            )
          ],
        ),
      ),
    );
  }
}
