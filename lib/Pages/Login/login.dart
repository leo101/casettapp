import 'package:casettapp/Pages/RegisterPage/registrati.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../Notifiers/mainNotifier.dart';

TextEditingController username = new TextEditingController();
TextEditingController password = new TextEditingController();
bool showPassword = false;
bool taken = false;
bool prefs = false;

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    if (!taken) {
      taken = true;
      Provider.of<MainNotifier>(context).getPrefs().then((value) {
        if (value[0] != null && value[1] != null) {
          username.text = value[0];
          password.text = value[1];
          setState(() {
            prefs = true;
          });
        } else {
          setState(() {
            prefs = false;
          });
        }
      });
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: prefs
          ? Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  CircularProgressIndicator(
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Colors.purple[300]),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Text(
                    'Accesso in corso...',
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
            )
          : Padding(
              padding:
                  const EdgeInsets.only(top: 128.0, left: 32.0, right: 32.0),
              child: Column(
                children: <Widget>[
                  Center(
                    child: Text(
                      'LOGIN',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 26.0),
                    ),
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  Icon(
                    Icons.account_circle,
                    size: 128.0,
                    color: Colors.blueGrey,
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  TextField(
                    controller: username,
                    decoration: InputDecoration(
                      hintText: 'Email',
                      suffixIcon: Icon(Icons.account_box),
                    ),
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  TextField(
                    onSubmitted: (_) => Provider.of<MainNotifier>(context)
                        .login(username.text, password.text),
                    controller: password,
                    decoration: InputDecoration(
                      hintText: 'Password',
                      suffixIcon: IconButton(
                        icon: Icon(showPassword
                            ? Icons.lock_outline
                            : Icons.lock_open),
                        onPressed: () {
                          setState(() {
                            showPassword = !showPassword;
                          });
                        },
                      ),
                    ),
                    obscureText: !showPassword,
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          Provider.of<MainNotifier>(context).errore,
                          style: TextStyle(color: Colors.grey),
                        ),
                        Provider.of<MainNotifier>(context).errore ==
                                'Password errata'
                            ? FlatButton(
                                child: Text('Dimenticato la password?'),
                                onPressed: () {
                                  Provider.of<MainNotifier>(context)
                                      .passwordDimenticata(username.text);
                                  showDialog(
                                    context: context,
                                    builder: (context) => SimpleDialog(
                                      title: Text('Email di reset inviata'),
                                    ),
                                  );
                                },
                              )
                            : Container()
                      ],
                    ),
                  ),
                  Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        FlatButton(
                          textColor: Colors.blue[800],
                          child: Text('ACCEDI'),
                          onPressed: () {
                            Provider.of<MainNotifier>(context)
                                .login(username.text, password.text);
                          },
                        ),
                        Text(
                          'oppure',
                          style: TextStyle(color: Colors.grey),
                        ),
                        FlatButton(
                          textColor: Colors.blue[800],
                          child: Text('Registrati'),
                          onPressed: () => Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => Registrati())),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
