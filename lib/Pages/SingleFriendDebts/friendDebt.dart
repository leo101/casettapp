import 'package:casettapp/Notifiers/mainNotifier.dart';
import 'package:casettapp/Pages/AddDebtPage/addDebtPage.dart';
import 'package:casettapp/Pages/EditDebt/editDebt.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

MainNotifier notifier;

class FriendDebt extends StatelessWidget {
  final int index;
  FriendDebt(this.index);

  @override
  Widget build(BuildContext context) {
    notifier = Provider.of<MainNotifier>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          notifier.user.friends[index].username ?? 'Caricamento...',
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => SimpleDialog(
                  title: Text(
                      'Eliminare ${notifier.user.friends[index].username} dagli amici?'),
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        FlatButton(
                          child: Text('Si'),
                          onPressed: () {
                            notifier.delFriend(index);
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          child: Text('No'),
                          onPressed: () => Navigator.pop(context),
                        ),
                      ],
                    )
                  ],
                ),
              );
            },
          )
        ],
      ),
      body: notifier.user.friends[index].debiti.length == 0
          ? Center(
              child: Text(
                'Nessun debito',
                style: TextStyle(color: Colors.grey),
              ),
            )
          : Padding(
              padding:
                  const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  notifier.user.friends[index].debiti.length > 0
                      ? Text(
                          'Tutti i debiti:',
                          style: TextStyle(color: Colors.grey),
                        )
                      : Container(),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: notifier.user.friends[index].debiti.length,
                    itemBuilder: (context, indexx) => Draggable(
                      axis: Axis.horizontal,
                      key: UniqueKey(),
                      maxSimultaneousDrags: 1,
                      child: Material(
                        child: ListTile(
                          title: Text(
                              '${notifier.user.friends[index].debiti[indexx].title}'),
                          subtitle: Text(
                              '${notifier.user.friends[index].debiti[indexx].value}€'),
                        ),
                      ),
                      childWhenDragging: Material(
                        clipBehavior: Clip.antiAlias,
                        borderRadius: BorderRadius.circular(8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width - 16,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              DragTarget(
                                key: Key('Edit'),
                                onAccept: (_) {
                                  Navigator.push(
                                      context,
                                      new MaterialPageRoute(
                                          builder: (context) => EditDebtPage(
                                                debtIndex: index,
                                                personIndex: this.index,
                                              )));
                                },
                                builder: (context, _, __) => Material(
                                  child: Container(
                                    height: 64.0,
                                    color: Colors.red,
                                    width:
                                        MediaQuery.of(context).size.width / 2 -
                                            16,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(
                                          width: 16.0,
                                        ),
                                        Text(
                                          'Elimina',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              DragTarget(
                                key: Key('Delete'),
                                onAccept: (_) {
                                  notifier.delDebt(this.index, index);
                                },
                                builder: (context, _, __) => Material(
                                  child: Container(
                                    color: Colors.blue[800],
                                    height: 64.0,
                                    width:
                                        MediaQuery.of(context).size.width / 2 -
                                            16,
                                    child: Center(
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          Text(
                                            'Modifica',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          SizedBox(
                                            width: 16.0,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      feedback: Material(
                        clipBehavior: Clip.antiAlias,
                        borderRadius: BorderRadius.circular(8),
                        child: Container(
                          width: MediaQuery.of(context).size.width - 16,
                          child: ListTile(
                            title: Text(
                                '${notifier.user.friends[index].debiti[indexx].title}'),
                            subtitle: Text(
                                '${notifier.user.friends[index].debiti[indexx].value}€'),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(Icons.add),
        label: Text('Aggiungi un debito'),
        onPressed: () => Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => AddDebtPage(
                      personIndex: index,
                    ))),
      ),
    );
  }
}

/*

Material(
                        clipBehavior: Clip.antiAlias,
                        borderRadius: BorderRadius.circular(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Material(
                              child: Container(
                                height: 64,
                                color: Colors.red,
                                width:
                                    (MediaQuery.of(context).size.width / 2) - 8,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(
                                      width: 16,
                                    ),
                                    Text(
                                      'Elimina',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Material(
                              child: Container(
                                color: Colors.blue[800],
                                height: 64,
                                width:
                                    (MediaQuery.of(context).size.width / 2) - 8,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      'Modifica',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    SizedBox(
                                      width: 16,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),

*/
