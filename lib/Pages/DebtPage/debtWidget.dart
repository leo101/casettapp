import 'package:casettapp/Notifiers/mainNotifier.dart';
import 'package:casettapp/Pages/SingleFriendDebts/friendDebt.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DebtWidget extends StatelessWidget {
  final int index;
  DebtWidget(this.index);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        new MaterialPageRoute(builder: (context) => FriendDebt(index)),
      ),
      child: ListTile(
        leading: Icon(Icons.euro_symbol),
        title: Text(
          Provider.of<MainNotifier>(context).user.friends[index].username ??
              'Caricamento...',
        ),
        subtitle: Text(
          'Debito totale: ${Provider.of<MainNotifier>(context).user.friends[index].totDebts()}',
        ),
      ),
    );
  }
}
