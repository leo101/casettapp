import 'package:casettapp/Notifiers/mainNotifier.dart';
import 'package:casettapp/Pages/AddFriend/addFriend.dart';
import 'package:casettapp/Pages/DebtPage/debtWidget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

MainNotifier notifier;

class DebtPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    notifier = Provider.of<MainNotifier>(context);
    return Scaffold(
      body: notifier.user.friends == null
          ? Center(
              child: Text(
                'Non hai amici',
                style: TextStyle(color: Colors.grey),
              ),
            )
          : notifier.user.friends.length == 0
              ? Center(
                  child: Text(
                    'Non hai amici',
                    style: TextStyle(color: Colors.grey),
                  ),
                )
              : ListView.builder(
                  padding:
                      EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
                  itemCount: notifier.user.friends.length,
                  itemBuilder: (context, index) => DebtWidget(index),
                ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.push(
            context, new MaterialPageRoute(builder: (context) => AddFriend())),
      ),
    );
  }
}
