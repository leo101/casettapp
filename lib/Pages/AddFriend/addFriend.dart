import 'package:casettapp/Models/friend.dart';
import 'package:casettapp/Notifiers/firebaseNotifier.dart';
import 'package:casettapp/Notifiers/mainNotifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

bool _searching = false;
TextEditingController _usernameSearch;
List<Friend> _amici = [];
List<Friend> _searchAmici = [];

class AddFriend extends StatefulWidget {
  @override
  _AddFriendState createState() => _AddFriendState();
}

class _AddFriendState extends State<AddFriend> {
  @override
  void initState() {
    _amici = [];
    _searchAmici = [];
    _usernameSearch = new TextEditingController();
    super.initState();
  }

  void getAmici(BuildContext context) async {
    _amici = await Provider.of<FirebaseNotifier>(context).getAmici();
    for (int i = 0;
        i < Provider.of<MainNotifier>(context).user.friends.length;
        i++) {
      for (int j = 0; j < _amici.length; j++)
        if (Provider.of<MainNotifier>(context).user.friends[i] == _amici[j]) {
          _amici.removeAt(j);
          break;
        }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (_amici.length == 0) getAmici(context);
    return Scaffold(
      appBar: AppBar(
        title: !_searching
            ? Text('Aggiungi un amico')
            : TextField(
                controller: _usernameSearch,
                autofocus: true,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Cerca',
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                ),
                onChanged: (value) {
                  _searchAmici = [];
                  _amici.forEach((e) {
                    if (e.username.contains(value)) _searchAmici.add(e);
                  });
                  setState(() {});
                },
              ),
        actions: <Widget>[
          !_searching
              ? IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    _searchAmici = _amici;
                    setState(() {
                      _searching = true;
                    });
                  },
                )
              : IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    setState(() {
                      _searching = false;
                      _searchAmici = [];
                    });
                  },
                ),
        ],
      ),
      body: ListView.builder(
        itemCount: !_searching ? _amici.length : _searchAmici.length,
        itemBuilder: (context, index) => ListTile(
          leading: Icon(Icons.account_circle),
          title: Text(!_searching
              ? _amici[index].username
              : _searchAmici[index].username),
          subtitle: Text(
              !_searching ? _amici[index].email : _searchAmici[index].email),
          onTap: () {
            showDialog(
              context: context,
              builder: (context) => SimpleDialog(
                title: Text(
                    'Aggiungere ${!_searching ? _amici[index].username : _searchAmici[index].username} agli amici?'),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                        child: Text('Si'),
                        onPressed: () {
                          Provider.of<MainNotifier>(context).addFriendOnline(
                              !_searching
                                  ? _amici[index]
                                  : _searchAmici[index]);
                          Navigator.pop(context);
                        },
                      ),
                      FlatButton(
                        child: Text('No'),
                        onPressed: () => Navigator.pop(context),
                      ),
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
