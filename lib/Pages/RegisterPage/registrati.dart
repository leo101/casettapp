import 'package:casettapp/Notifiers/mainNotifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

TextEditingController username;
TextEditingController password;
TextEditingController passwordCheck;
TextEditingController email;

class Registrati extends StatefulWidget {
  @override
  _RegistratiState createState() => _RegistratiState();
}

class _RegistratiState extends State<Registrati> {
  @override
  void initState() {
    username = new TextEditingController();
    password = new TextEditingController();
    passwordCheck = new TextEditingController();
    email = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registrati'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: TextField(
                decoration: InputDecoration(
                  border: InputBorder.none,
                  fillColor: Colors.blue[800],
                  filled: true,
                  hintText: 'Email',
                  hintStyle: TextStyle(color: Colors.grey[300]),
                ),
                keyboardType: TextInputType.emailAddress,
                style: TextStyle(color: Colors.white),
                controller: email,
              ),
            ),
            SizedBox(
              height: 8.0,
            ),
            Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: TextField(
                decoration: InputDecoration(
                  border: InputBorder.none,
                  fillColor: Colors.blue[800],
                  filled: true,
                  hintText: 'Username',
                  hintStyle: TextStyle(color: Colors.grey[300]),
                ),
                keyboardType: TextInputType.text,
                style: TextStyle(color: Colors.white),
                controller: username,
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  fillColor: Colors.blue[800],
                  filled: true,
                  hintText: 'Password',
                  hintStyle: TextStyle(color: Colors.grey[300]),
                ),
                keyboardType: TextInputType.text,
                style: TextStyle(color: Colors.white),
                controller: password,
              ),
            ),
            SizedBox(
              height: 8.0,
            ),
            Material(
              borderRadius: BorderRadius.circular(8.0),
              clipBehavior: Clip.antiAlias,
              child: Theme(
                data: new ThemeData(
                  primaryColor: password.text != passwordCheck.text
                      ? Colors.redAccent
                      : password.text != null || password.text != ''
                          ? Colors.green
                          : null,
                ),
                child: TextField(
                  onChanged: (_) {
                    setState(() {});
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      borderSide: BorderSide(
                        color: password.text != passwordCheck.text
                            ? Colors.redAccent
                            : Colors.transparent,
                      ),
                    ),
                    fillColor: Colors.blue[800],
                    filled: true,
                    hintText: 'Conferma la password',
                    hintStyle: TextStyle(color: Colors.grey[300]),
                  ),
                  keyboardType: TextInputType.text,
                  style: TextStyle(color: Colors.white),
                  controller: passwordCheck,
                ),
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                OutlineButton(
                  borderSide: BorderSide(color: Colors.blue[800]),
                  textColor: Colors.blue[800],
                  child: Text(
                    'Registrati',
                    style: TextStyle(fontSize: 15.0),
                  ),
                  onPressed: () {
                    if (password.text == passwordCheck.text &&
                        username.text.trim() != '' &&
                        username.text.trim() != null &&
                        email.text.trim() != '' &&
                        email.text.trim() != null)
                      Provider.of<MainNotifier>(context)
                          .registrati(email.text, password.text, username.text);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
