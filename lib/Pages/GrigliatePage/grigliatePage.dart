import 'package:casettapp/Notifiers/grigliataNotifier.dart';
import 'package:casettapp/Notifiers/mainNotifier.dart';

import 'package:casettapp/Pages/GrigliatePage/editGrigliata.dart';
import 'package:casettapp/Pages/GrigliatePage/newGrigliata.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

GrigliataNotifier _notifier;

class GrigliatePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    _notifier = Provider.of<GrigliataNotifier>(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: _notifier.grigliata != null
            ? ListView(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text('Programmata per il ' +
                          '${_notifier.grigliata.data.toDate().day}/' +
                          '${_notifier.grigliata.data.toDate().month}/' +
                          '${_notifier.grigliata.data.toDate().year}'),
                      SizedBox(
                        width: 16.0,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  _notifier.grigliata.cibo.length > 0
                      ? Text(
                          'Cibo',
                          style: TextStyle(
                              color: Colors.grey, fontWeight: FontWeight.w600),
                        )
                      : Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              height: 16.0,
                            ),
                            Text(
                              'Non hai ancora inserito cosa mangiare',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ],
                        ),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: _notifier.grigliata.cibo.length,
                    itemBuilder: (context, index) => Container(
                      padding: EdgeInsets.only(bottom: 16.0),
                      child: ListTile(
                        title: Text(_notifier.grigliata.cibo[index].nome),
                        subtitle: Text(
                            _notifier.grigliata.cibo[index].peso == null
                                ? _notifier.grigliata.cibo[index].quantita
                                        .toString() +
                                    ' pezzi'
                                : _notifier.grigliata.cibo[index].peso + 'Kg'),
                        trailing: Checkbox(
                          value: _notifier.grigliata.cibo[index].preso,
                          onChanged: (value) =>
                              _notifier.setCiboPreso(index, value),
                        ),
                      ),
                    ),
                  ),
                  _notifier.grigliata.invitati.length > 0
                      ? Text(
                          'INVITATI',
                          style: TextStyle(
                              color: Colors.grey, fontWeight: FontWeight.w600),
                        )
                      : Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              height: 16.0,
                            ),
                            Text(
                              'Non hai ancora invitato nessuno',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ],
                        ),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: _notifier.grigliata.invitati.length,
                    itemBuilder: (context, index) => Container(
                      padding: EdgeInsets.only(bottom: 16.0),
                      child: ListTile(
                        title: Text(_notifier.grigliata.invitati[index].nome),
                        trailing: Provider.of<MainNotifier>(context)
                                    .user
                                    .username
                                    .toLowerCase() ==
                                _notifier.grigliata.invitati[index].nome
                                    .toLowerCase()
                            ? Tooltip(
                                message: 'Modifica la presenza',
                                child: Checkbox(
                                  value: _notifier
                                      .grigliata.invitati[index].accettato,
                                  onChanged: (value) => _notifier
                                      .setPersonaAccepted(index, value),
                                ),
                              )
                            : Tooltip(
                                message: _notifier.grigliata.invitati[index]
                                            .accettato ==
                                        null
                                    ? 'Boh'
                                    : _notifier
                                            .grigliata.invitati[index].accettato
                                        ? 'Presente'
                                        : 'Non presente',
                                child: IconButton(
                                  icon: Icon(Icons.fiber_manual_record,
                                      color: _notifier.grigliata.invitati[index]
                                                  .accettato ==
                                              null
                                          ? Colors.orange
                                          : _notifier.grigliata.invitati[index]
                                                  .accettato
                                              ? Colors.green
                                              : Colors.red),
                                  onPressed: () {},
                                ),
                              ),
                      ),
                    ),
                  ),
                ],
              )
            : Center(
                child: Text(
                  'Nessuna grigliata programmata',
                  style: TextStyle(color: Colors.grey),
                ),
              ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(_notifier.grigliata == null ? Icons.add : Icons.edit),
        label:
            Text(_notifier.grigliata == null ? 'Nuova grigliata' : 'Modifica'),
        onPressed: () => Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => _notifier.grigliata == null
                    ? NewGrigliata()
                    : EditGrigliata())),
      ),
    );
  }
}
