import 'package:casettapp/Models/cibo.dart';
import 'package:casettapp/Notifiers/grigliataNotifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

GrigliataNotifier notifier;
TextEditingController nome = new TextEditingController();
TextEditingController quantita = new TextEditingController();
TextEditingController peso = new TextEditingController();

class AddCibo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    notifier = Provider.of<GrigliataNotifier>(context);
    if (nome == null) {
      nome = new TextEditingController();
      peso = new TextEditingController();
      quantita = new TextEditingController();
    }
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 64.0, left: 16.0, right: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Inserisci un nuovo alimento',
              style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 32.0,
            ),
            TextField(
              controller: nome,
              autofocus: true,
              textCapitalization: TextCapitalization.sentences,
              decoration: InputDecoration(
                hintText: 'Nome',
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            Flex(
              direction: Axis.horizontal,
              children: <Widget>[
                Flexible(
                  flex: 2,
                  child: TextField(
                    controller: peso,
                    decoration: InputDecoration(
                      hintText: 'Peso',
                    ),
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: Container(
                    width: double.infinity,
                    child: Center(
                      child: Text('Oppure'),
                    ),
                  ),
                ),
                Flexible(
                  flex: 2,
                  child: TextField(
                    controller: quantita,
                    decoration: InputDecoration(hintText: 'Quantità'),
                    keyboardType: TextInputType.number,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            mini: true,
            heroTag: '',
            backgroundColor: Colors.redAccent,
            onPressed: () {
              Navigator.pop(context);
              nome = null;
            },
            child: Icon(Icons.undo),
          ),
          FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              if (nome.text != '' && (peso.text != '' || quantita.text != '')) {
                notifier.addCibo(new Cibo(
                  nome: nome.text,
                  peso: peso.text == '' ? null : peso.text,
                  preso: false,
                  quantita:
                      quantita.text == '' ? null : int.parse(quantita.text),
                ));
                Navigator.pop(context);
                nome = null;
              } else if (nome.text == '') {
                Toast.show(
                  'Inserisci il nome dell\'alimento',
                  context,
                  backgroundColor: Colors.grey,
                  gravity: 4,
                );
              } else if (peso.text == '' || quantita.text == '')
                Toast.show(
                  'Inserisci il peso o la quantità',
                  context,
                  backgroundColor: Colors.grey,
                  gravity: 4,
                );
            },
          ),
        ],
      ),
    );
  }
}
