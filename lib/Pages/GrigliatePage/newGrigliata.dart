import 'package:casettapp/Models/grigliata.dart';
import 'package:casettapp/Notifiers/grigliataNotifier.dart';
import 'package:casettapp/Notifiers/mainNotifier.dart';
import 'package:casettapp/Pages/GrigliatePage/editGrigliata.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

GrigliataNotifier notifier;
DateTime data;

class NewGrigliata extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    notifier = Provider.of<GrigliataNotifier>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Nuova grigliata'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.fastfood,
              size: 128.0,
              color: Colors.grey,
            ),
            SizedBox(
              height: 8.0,
            ),
            RaisedButton(
              color: Colors.blue[800],
              textColor: Colors.white,
              child: Text('Avvia la grigliata'),
              onPressed: () async {
                data = await showDatePicker(
                  context: context,
                  firstDate: DateTime(DateTime.now().year, DateTime.now().month,
                      DateTime.now().day - 1),
                  initialDate: data ?? DateTime.now(),
                  lastDate: DateTime(DateTime.now().year + 2,
                      DateTime.now().month, DateTime.now().day),
                );

                if (data != null) {
                  notifier.newGrigliata(
                      Grigliata(
                          cibo: [],
                          invitati: [],
                          data: Timestamp.fromDate(data)),
                      Provider.of<MainNotifier>(context).user.username);
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (context) => EditGrigliata(),
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
