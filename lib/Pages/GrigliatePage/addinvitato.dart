import 'package:casettapp/Models/invitato.dart';

import 'package:casettapp/Notifiers/grigliataNotifier.dart';
import 'package:casettapp/Notifiers/mainNotifier.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

MainNotifier _mainNotifier;
GrigliataNotifier _grigliataNotifier;
List<Map<String, bool>> _selected = [];

class AddInvitato extends StatefulWidget {
  @override
  _AddInvitatoState createState() => _AddInvitatoState();
}

class _AddInvitatoState extends State<AddInvitato> {
  @override
  void initState() {
    super.initState();
  }

  void set(BuildContext context) {
    _grigliataNotifier = Provider.of<GrigliataNotifier>(context);
    _mainNotifier = Provider.of<MainNotifier>(context);
    _selected = [];
    _mainNotifier.user.friends.forEach((e) {
      _selected.add({e.username: false});
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_mainNotifier == null) {
      print('MainNotifier is null');
      set(context);
    }
    print(
        'length: ${_selected.length} friend length: ${_mainNotifier.user.friends.length}');
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 64.0, left: 16.0, right: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Aggiungi un invitato',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24.0),
            ),
            ListView.builder(
              shrinkWrap: true,
              itemCount: _mainNotifier.user.friends.length,
              itemBuilder: (context, index) => isPresent(index)
                  ? Container()
                  : Container(
                      padding: EdgeInsets.only(bottom: 8.0),
                      child: ListTile(
                        title: Text(_mainNotifier.user.friends[index].username),
                        trailing: Checkbox(
                          value: _selected[index]
                              [_mainNotifier.user.friends[index].username],
                          onChanged: (value) {
                            setState(() {
                              _selected[index][_mainNotifier
                                  .user.friends[index].username] = value;
                            });
                          },
                        ),
                      ),
                    ),
            ),
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            mini: true,
            heroTag: '',
            backgroundColor: Colors.redAccent,
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.undo),
          ),
          FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              _selected.forEach((e) {
                if (e.values.first == true) {
                  _grigliataNotifier
                      .addInvitato(new Invitato(nome: e.keys.first));
                }
              });
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }

  bool isPresent(int index) {
    bool isPresent = false;
    _grigliataNotifier.grigliata.invitati.forEach((e) {
      if (e.nome == _mainNotifier.user.friends[index].username)
        isPresent = true;
    });
    return isPresent;
  }
}
