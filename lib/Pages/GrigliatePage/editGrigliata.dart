import 'package:casettapp/Notifiers/grigliataNotifier.dart';
import 'package:casettapp/Notifiers/pageNotifier.dart';
import 'package:casettapp/Pages/GrigliatePage/addCibo.dart';
import 'package:casettapp/Pages/GrigliatePage/addinvitato.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:casettapp/Notifiers/mainNotifier.dart';

GrigliataNotifier grigliataNotifier;
PageNotifier pageNotifier;

class EditGrigliata extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    grigliataNotifier = Provider.of<GrigliataNotifier>(context);
    pageNotifier = Provider.of<PageNotifier>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Modifica la grigliata'),
        actions: <Widget>[
          Tooltip(
            message: 'Elimina la grigliata',
            child: IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) => SimpleDialog(
                    title: Text('Eliminare la grigliata?'),
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          FlatButton(
                            child: Text('Si'),
                            onPressed: () {
                              grigliataNotifier.delGrigliata();
                              Navigator.pop(context);
                              Navigator.pop(context);
                            },
                          ),
                          FlatButton(
                            onPressed: () => Navigator.pop(context),
                            child: Text('No'),
                          ),
                        ],
                      )
                    ],
                  ),
                );
              },
            ),
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.fastfood),
            title: Text('Cibo'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.supervisor_account),
            title: Text('Invitati'),
          ),
        ],
        currentIndex: pageNotifier.editGrigliataPage,
        onTap: (index) => pageNotifier.changeGrigliataPage(index),
      ),
      body: pageNotifier.editGrigliataPage == 0 ? _CiboPage() : _InvitatiPage(),
    );
  }
}

class _CiboPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.push(
            context, new MaterialPageRoute(builder: (context) => AddCibo())),
      ),
      body: grigliataNotifier.grigliata.cibo.length == 0
          ? Center(
              child: Text(
              'Nessun cibo inserito',
              style: TextStyle(color: Colors.grey),
            ))
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView.builder(
                itemCount: grigliataNotifier.grigliata.cibo.length,
                itemBuilder: (context, index) => Container(
                  padding: EdgeInsets.only(bottom: 16.0),
                  child: ListTile(
                    title: Text(grigliataNotifier.grigliata.cibo[index].nome),
                    subtitle: Text(
                        grigliataNotifier.grigliata.cibo[index].peso == null
                            ? grigliataNotifier.grigliata.cibo[index].quantita
                                    .toString() +
                                ' pezzi'
                            : grigliataNotifier.grigliata.cibo[index].peso +
                                'Kg'),
                    trailing: IconButton(
                      icon: Icon(
                        Icons.remove,
                        color: Colors.black,
                      ),
                      onPressed: () => grigliataNotifier.delCibo(index),
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}

class _InvitatiPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.push(context,
            new MaterialPageRoute(builder: (context) => AddInvitato())),
      ),
      body: grigliataNotifier.grigliata.invitati.length == 0
          ? Center(
              child: Text(
                'Nessun invitato',
                style: TextStyle(color: Colors.grey),
              ),
            )
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView.builder(
                itemCount: grigliataNotifier.grigliata.invitati.length,
                itemBuilder: (context, index) => Container(
                  padding: EdgeInsets.only(bottom: 16.0),
                  child: ListTile(
                    title: Text(
                      grigliataNotifier.grigliata.invitati[index].nome,
                    ),
                    subtitle: Text(grigliataNotifier
                                .grigliata.invitati[index].nome
                                .toLowerCase() ==
                            Provider.of<MainNotifier>(context)
                                .user
                                .username
                                .toLowerCase()
                        ? ''
                        : grigliataNotifier
                                    .grigliata.invitati[index].accettato ==
                                null
                            ? 'Boh'
                            : grigliataNotifier
                                    .grigliata.invitati[index].accettato
                                ? 'Presente'
                                : 'Non presente'),
                    trailing: grigliataNotifier.grigliata.invitati[index].nome
                                .toLowerCase() ==
                            Provider.of<MainNotifier>(context)
                                .user
                                .username
                                .toLowerCase()
                        ? null
                        : IconButton(
                            icon: Icon(Icons.remove),
                            onPressed: () =>
                                grigliataNotifier.delInvitato(index),
                          ),
                  ),
                ),
              ),
            ),
    );
  }
}
