import 'package:casettapp/Notifiers/mainNotifier.dart';
import 'package:casettapp/Notifiers/pageNotifier.dart';

import 'package:casettapp/Pages/AccountPage/accountPage.dart';
import 'package:casettapp/Pages/CreditiPage/creditiPage.dart';
import 'package:casettapp/Pages/DebtPage/debtPage.dart';
import 'package:casettapp/Pages/GrigliatePage/grigliatePage.dart';
import 'package:casettapp/Pages/PizzePage/pizzePage.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

PageNotifier pageNotifier;
MainNotifier mainNotifier;

List<Widget> pages = [
  AccountPage(),
  DebtPage(),
  CreditiPage(),
  PizzePage(),
  GrigliatePage(),
];

class SkeletonPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    pageNotifier = Provider.of<PageNotifier>(context);
    mainNotifier = Provider.of<MainNotifier>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(pageNotifier.pageName()),
      ),
      drawer: Drawer(
        child: Padding(
          padding: const EdgeInsets.only(right: 8.0, top: 8.0, bottom: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 32.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      mainNotifier.user == null
                          ? ''
                          : mainNotifier.user.username
                                  .substring(0, 1)
                                  .toUpperCase() +
                              mainNotifier.user.username.substring(1),
                      style: TextStyle(
                          fontSize: 26.0, fontWeight: FontWeight.bold),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.account_circle,
                        size: 28.0,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                        pageNotifier.changePage(0);
                      },
                    ),
                  ],
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                child: Text(
                  'PAGINE',
                  style: TextStyle(
                      color: Colors.grey, fontWeight: FontWeight.w400),
                ),
              ),
              Container(
                constraints: BoxConstraints(maxHeight: 512.0),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: pages.length - 1,
                  itemBuilder: (context, index) {
                    index += 1;
                    return GestureDetector(
                      onTap: () {
                        pageNotifier.changePage(index);
                        Navigator.pop(context);
                      },
                      child: Container(
                        height: 50.0,
                        child: Material(
                          clipBehavior: Clip.antiAlias,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.zero,
                              topLeft: Radius.zero,
                              bottomRight: Radius.circular(24.0),
                              topRight: Radius.circular(24.0)),
                          child: Container(
                            color: pageNotifier.pageIndex == index
                                ? Colors.blue[100]
                                : Colors.transparent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  width: 16.0,
                                ),
                                Text(
                                  pageNotifier.pageName(index: index),
                                  style: TextStyle(
                                      color: pageNotifier.pageIndex == index
                                          ? Colors.blue[900]
                                          : Colors.black,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      body: pages[pageNotifier.pageIndex],
    );
  }
}
