import 'package:casettapp/Notifiers/mainNotifier.dart';
import 'package:casettapp/Notifiers/pageNotifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AccountPage extends StatefulWidget {
  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Center(
              child: Column(
                children: <Widget>[
                  Icon(
                    Icons.account_circle,
                    size: 128.0,
                    color: Colors.blueGrey,
                  ),
                  Text(
                    Provider.of<MainNotifier>(context).user.username,
                    style: TextStyle(
                      fontSize: 22.0,
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.redAccent,
        foregroundColor: Colors.white,
        label: Text('Logout'),
        icon: Icon(Icons.exit_to_app),
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => SimpleDialog(
              title: Text('Eseguire il logout?'),
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      child: Text('Si'),
                      onPressed: () {
                        Navigator.pop(context);
                        Provider.of<MainNotifier>(context).logout();
                        Provider.of<PageNotifier>(context).changePage(1);
                      },
                    ),
                    FlatButton(
                      child: Text('No'),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
