class Cibo {
  String nome;
  int quantita;
  String peso;
  bool preso;
  String path;

  Cibo({
    this.nome,
    this.quantita,
    this.peso,
    this.preso,
    this.path,
  });

  factory Cibo.fromJSON(Map<String, dynamic> json, String path) => Cibo(
        nome: json['Nome'],
        peso: json['Peso'],
        preso: json['Preso'],
        quantita: json['Quantita'],
        path: path,
      );

  Map<String, dynamic> toJSON() {
    return {
      'Nome': nome,
      'Peso': peso,
      'Preso': preso,
      'Quantita': quantita,
    };
  }
}
