class Debt {
  double value; //Il valore del debito
  String emailPerson; //persona a cui si devono i soldi
  String title; //Incipit del debito (ex. Oggetto comprato)
  String description; //Dettagli sul debito
  String location;

  Debt({
    this.value,
    this.title,
    this.description,
    this.emailPerson,
    this.location,
  });

  factory Debt.fromJSON(Map<String, dynamic> json, String location) => Debt(
        description: json['Descrizione'],
        value: json['Importo'],
        title: json['Titolo'],
        emailPerson: json['Email'],
        location: location,
      );
  Map<String, dynamic> toJSON() {
    return {
      'Descrizione': description,
      'Importo': value,
      'Titolo': title,
      'email': emailPerson,
    };
  }
}
