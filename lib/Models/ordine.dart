import 'package:casettapp/Models/pizza.dart';

class Ordine {
  List<Pizza> pizza;
  String nome;
  bool payed = false;

  Ordine({
    this.nome,
    this.pizza,
    this.payed,
  });

  double getTot() {
    double tot = 0;
    pizza.forEach((e) {
      tot += e.cost;
    });
    return tot;
  }
}
