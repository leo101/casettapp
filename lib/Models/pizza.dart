class Pizza {
  String name;
  double cost;
  String path;

  Pizza({
    this.cost,
    this.name,
    this.path,
  });

  factory Pizza.fromJSON(Map<String, dynamic> json, String path) => Pizza(
        cost: double.parse(json['Costo']),
        name: json['Nome'].toString().trim(),
        path: path,
      );

  Map<String, dynamic> toJSON() {
    return {
      'Nome': name,
      'Costo': cost,
    };
  }
}
