import 'package:casettapp/Models/debt.dart';

class Friend {
  String username;
  String email;
  List<Debt> debiti = [];

  double totDebts() {
    double tot = 0.0;
    debiti.forEach((e) {
      tot += e.value;
    });
    return tot;
  }

  Friend({
    this.username,
    this.debiti,
    this.email,
  });

  factory Friend.fromString(String email, String username) {
    return Friend(debiti: [], email: email, username: username);
  }
}
