class Invitato {
  String nome;
  bool accettato;
  String path;

  factory Invitato.fromJSON(Map<String, dynamic> json, String path) => Invitato(
        accettato: json['Presente'],
        nome: json['Nome'],
        path: path,
      );

  Map<String, dynamic> toJSON() {
    return {'Nome': nome, 'Presente': accettato};
  }

  Invitato({
    this.nome,
    this.accettato,
    this.path,
  });
}
