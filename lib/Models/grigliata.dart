import 'package:casettapp/Models/invitato.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'cibo.dart';

class Grigliata {
  Timestamp data;
  List<Invitato> invitati;
  List<Cibo> cibo;
  String path;

  Grigliata({
    this.data,
    this.invitati,
    this.cibo,
    this.path,
  });

  Map<String, dynamic> toJSON() {
    return {'Data': data};
  }

  factory Grigliata.fromJSON(Map<String, dynamic> json, String path,
          {List<Cibo> cibo, List<Invitato> invitati}) =>
      Grigliata(
        data: json['Data'],
        cibo: cibo ?? [],
        invitati: invitati ?? [],
        path: path,
      );
}
