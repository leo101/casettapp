import 'friend.dart';

class User {
  String username;
  List<Friend> friends = [];
  String imageURL;
  String email;
  String password;

  User({
    this.username,
    this.friends,
    this.imageURL,
    this.email,
    this.password,
  });

  factory User.fromJSON(Map<String, dynamic> json) {
    List<dynamic> friends = json['FriendList'];
    return User(
      username: json['username'] ?? [],
      friends: List.generate(
        friends.length,
        (index) => new Friend(email: friends[index]),
      ),
    );
  }
}
