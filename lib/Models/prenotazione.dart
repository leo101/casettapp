import 'ordine.dart';

class Prenotazione {
  List<Ordine> lista = [];

  Prenotazione({
    this.lista,
  });

  String getPizzeNames(int orderIndex) {
    String pizze = '';
    lista[orderIndex].pizza.forEach((e) {
      pizze += "\t\t\t\t" + e.name + '\n';
    });
    return pizze;
  }

  int getItemNumberFromIndex(int orderIndex) {
    return lista[orderIndex].pizza.length;
  }

  double getTotPrenotation() {
    double tot = 0;
    lista.forEach((e) {
      tot += e.getTot();
    });
    return tot;
  }

  double getTot(int orderIndex) {
    double tot = 0;
    lista[orderIndex].pizza.forEach((e) {
      tot += e.cost;
    });
    return tot;
  }
}
