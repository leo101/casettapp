import 'package:casettapp/Models/debt.dart';
import 'package:casettapp/Models/friend.dart';
import 'package:casettapp/Models/pizza.dart';
import 'package:casettapp/Models/user.dart';
import 'package:casettapp/Notifiers/pizzeNotifier.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'firebaseNotifier.dart';
import 'grigliataNotifier.dart';

class MainNotifier with ChangeNotifier {
  bool loggedIn = false;
  User user;
  BuildContext context;
  MainNotifier({this.context});
  String errore = '';

  final FirebaseAuth _auth = FirebaseAuth.instance;

  void savePrefs(String email, String password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('email', email);
    prefs.setString('drowssap', password);
    prefs = null;
  }

  void registrati(String email, String password, String username) async {
    await _auth.createUserWithEmailAndPassword(
        email: email, password: password);
    login(email, password);
  }

  void login(String email, String password) {
    _auth
        .signInWithEmailAndPassword(email: email, password: password)
        .then((response) async {
      errore = '';
      savePrefs(email, password);
      loggedIn = true;
      Map<String, dynamic> ex =
          await Provider.of<FirebaseNotifier>(context).download(email);
      List<dynamic> e = ex['FriendList'];
      user = User(
        email: email,
        username: ex['Username'],
        password: password,
        friends: [],
      );
      notifyListeners();
      //Aggiunge gli amici
      e.forEach((e) {
        addFriend(
            friend: new Friend.fromString(
                e, Provider.of<FirebaseNotifier>(context).getUsername(e)));
      });
      //Aggiunge i debiti
      user.friends.forEach((e) async {
        e.debiti =
            await Provider.of<FirebaseNotifier>(context).getDebts(e.email) ??
                [];
      });
      //Aggiunge le pizze
      Provider.of<FirebaseNotifier>(context).getPizze().then((value) {
        value.documents.forEach((e) {
          Provider.of<PizzeNotifier>(context)
              .addPizza(new Pizza.fromJSON(e.data, e.reference.path));
        });
      });
      //Aggiunge la grigliata
      Provider.of<GrigliataNotifier>(context)
          .addGrl(await Provider.of<FirebaseNotifier>(context).getGrigliata());
    }).catchError((error) {
      print('Errore: $error');
      errore = error.toString().substring(
          error.toString().indexOf('(') + 1, error.toString().indexOf(','));
      if (errore == 'ERROR_WRONG_PASSWORD') {
        errore = 'Password errata';
      }
      notifyListeners();
    });
  }

  Future<List<String>> getPrefs() async {
    List<String> prefs = [];
    SharedPreferences prefsc = await SharedPreferences.getInstance();
    prefs.add(prefsc.getString('email'));
    prefs.add(prefsc.getString('drowssap'));
    if (prefs[0] != null && prefs[1] != null) {
      login(prefs[0], prefs[1]);
    } else {
      prefs = [];
    }
    return prefs;
  }

  void passwordDimenticata(String email) {
    _auth.sendPasswordResetEmail(email: email);
  }

  void modDebt(int personIndex, int debtIndex, Debt nDebt) {
    user.friends[personIndex].debiti[debtIndex] = nDebt;
    notifyListeners();
  }

  void addFriend({Friend friend}) {
    user.friends.add(friend);
    notifyListeners();
  }

  void delFriend(int personaIndex) {
    user.friends.removeAt(personaIndex);
    notifyListeners();
    Provider.of<FirebaseNotifier>(context).delAmico(utente: user);
  }

  void addFriendOnline(Friend friend) {
    addFriend(friend: friend);
    Provider.of<FirebaseNotifier>(context).addAmico(utente: user);
  }

  void delDebt(int personIndex, int index) {
    Provider.of<FirebaseNotifier>(context)
        .delDebt(user.friends[personIndex].debiti[index].location);
    user.friends[personIndex].debiti.removeAt(index);
    notifyListeners();
  }

  void addDebt(Debt debito, int index) async {
    debito.location = await Provider.of<FirebaseNotifier>(context)
        .addDebt(debito, user.email);
    user.friends[index].debiti.add(debito);
    notifyListeners();
  }

  void logout() async {
    user = null;
    loggedIn = false;
    SharedPreferences prefsc = await SharedPreferences.getInstance();
    prefsc.clear();
    _auth.signOut();
    notifyListeners();
  }
}
