import 'package:casettapp/Models/cibo.dart';
import 'package:casettapp/Models/debt.dart';
import 'package:casettapp/Models/friend.dart';
import 'package:casettapp/Models/grigliata.dart';
import 'package:casettapp/Models/invitato.dart';
import 'package:casettapp/Models/pizza.dart';
import 'package:casettapp/Models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FirebaseNotifier with ChangeNotifier {
  QuerySnapshot snap;
  QuerySnapshot debiti;
  Map<String, dynamic> user;
  //Scarica l'intero database degli utenti
  Future<Map<String, dynamic>> download(String email) async {
    snap = await Firestore.instance.collection('users').getDocuments();
    debiti = await Firestore.instance
        .collection('users')
        .document(email)
        .collection('Debiti')
        .getDocuments();
    user = snap.documents.firstWhere((e) {
      return e.data['Email'] == email;
    }).data;
    return user;
  }

  Future<List<Friend>> getAmici() async {
    QuerySnapshot amici =
        await Firestore.instance.collection('users').getDocuments();
    List<Friend> amiki = [];
    amici.documents.forEach((e) {
      if (e.data['Username'] != user['Username'])
        amiki.add(new Friend.fromString(e.data['Email'], e.data['Username']));
    });
    return amiki;
  }

  Future<String> addPizza(Pizza pizza) {
    Firestore.instance.collection('Pizze').add(pizza.toJSON()).then((response) {
      return response.path;
    });
  }

  void addAmico({User utente}) {
    Firestore.instance.collection('users').document(utente.email).updateData({
      'FriendList': List.generate(
          utente.friends.length, (index) => utente.friends[index].email),
      'Email': utente.email,
      'Username': utente.username,
    });
  }

  void delAmico({User utente}) {
    Firestore.instance.collection('users').document(utente.email).updateData({
      'FriendList': List.generate(
          utente.friends.length, (index) => utente.friends[index].email),
      'Email': utente.email,
      'Username': utente.username,
    });
  }

  Future<String> addDebt(Debt debito, String email) async {
    Firestore.instance
        .collection('users')
        .document(email)
        .collection('Debiti')
        .add(debito.toJSON())
        .then((reference) {
      return reference.path;
    });
  }

  Future<Grigliata> getGrigliata() async {
    Grigliata grigliata = Grigliata(invitati: []);
    QuerySnapshot q =
        await Firestore.instance.collection('Grigliata').getDocuments();
    if (q.documents.length > 0) {
      grigliata.path = q.documents[0].reference.path;
      QuerySnapshot cibo = await Firestore.instance
          .document(grigliata.path)
          .collection('Cibo')
          .getDocuments();
      QuerySnapshot invitati = await Firestore.instance
          .document(grigliata.path)
          .collection('Invitati')
          .getDocuments();
      grigliata = null;
      grigliata = new Grigliata.fromJSON(
        q.documents[0].data,
        q.documents[0].reference.path,
        cibo: List.generate(
          cibo.documents.length,
          (index) => Cibo.fromJSON(
              cibo.documents[index].data, cibo.documents[index].reference.path),
        ),
        invitati: List.generate(
          invitati.documents.length,
          (index) => Invitato.fromJSON(invitati.documents[index].data,
              invitati.documents[index].reference.path),
        ),
      );
    }
    bool sonoDentro = false;
    grigliata.invitati.forEach((e) {
      if (e.nome == user['Username']) sonoDentro = true;
    });
    return sonoDentro ? grigliata : null;
  }

  void setPersonaAccettato(String path, Invitato invitato) {
    Firestore.instance.document(path).updateData(invitato.toJSON());
  }

  Future<String> newGrigliata(Grigliata grigliata) async {
    Firestore.instance
        .collection('Grigliata')
        .add(grigliata.toJSON())
        .then((response) {
      Firestore.instance
          .document(response.path)
          .collection('Invitati')
          .add(grigliata.invitati[0].toJSON());
      return response.path;
    });
  }

  Future<String> addInvitato(Invitato invitato, String grigliataPath) {
    Firestore.instance
        .document(grigliataPath)
        .collection('Invitati')
        .add(invitato.toJSON())
        .then((response) {
      return response.path;
    });
  }

  void delDocument(String path) {
    Firestore.instance.document(path).delete();
  }

  void setCiboPreso(String path, Cibo cibo) {
    Firestore.instance.document(path).updateData(cibo.toJSON());
  }

  Future<String> addCibo(Cibo cibo) async {
    Firestore.instance.collection('Grigliata').getDocuments().then((value) {
      Firestore.instance
          .document(value.documents[0].reference.path)
          .collection('Cibo')
          .add(cibo.toJSON())
          .then((response) {
        return response.path;
      });
    });
  }

  void delDebt(String location) {
    Firestore.instance.document(location).delete();
  }

//Trova l'username dell'amico
  String getUsername(String userEmail) {
    String a = snap.documents.firstWhere((e) {
      return e.data['Email'] == userEmail;
    }).data['Username'];
    return a;
  }

//Trova i debiti verso un singolo amico
  Future<List<Debt>> getDebts(String userEmail) async {
    List<Debt> debiti = [];
    this.debiti.documents.forEach((e) {
      if (e.data['email'] == userEmail) {
        debiti.add(new Debt.fromJSON(e.data, e.reference.path));
      }
    });
    return debiti;
  }

  Future<QuerySnapshot> getPizze() async {
    return await Firestore.instance.collection('Pizze').getDocuments();
  }
}
