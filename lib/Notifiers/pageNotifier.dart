import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class PageNotifier with ChangeNotifier {
  int pageIndex = 1;
  int editGrigliataPage = 0;
  List<String> _pageNames = [
    'Account',
    'Debiti',
    'Crediti',
    'Pizze',
    'Grigliate',
  ];

  void changeGrigliataPage(int nIndex) {
    editGrigliataPage = nIndex;
    notifyListeners();
  }

  void changePage(int newIndex) {
    pageIndex = newIndex;
    notifyListeners();
  }

  String pageName({int index}) {
    return _pageNames[index ?? pageIndex];
  }
}
