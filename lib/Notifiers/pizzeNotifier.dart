import 'package:casettapp/Models/ordine.dart';
import 'package:casettapp/Models/pizza.dart';
import 'package:casettapp/Models/prenotazione.dart';
import 'package:casettapp/Notifiers/firebaseNotifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class PizzeNotifier with ChangeNotifier {
  BuildContext context;
  PizzeNotifier({this.context});

  //Lista di pizze
  List<Pizza> pizze = [
    Pizza(cost: 4.0, name: 'Margherita'),
  ];

  Prenotazione prenotazione = new Prenotazione(lista: []);

  void delPrenotazioni() {
    prenotazione.lista = [];
    notifyListeners();
  }

  void setPayedOrNot(int orderIndex, bool payed) {
    prenotazione.lista[orderIndex].payed = payed;
    notifyListeners();
  }

  void addOrdine(Ordine ordine) {
    bool found = false;
    prenotazione.lista.forEach((e) {
      if (e.nome == ordine.nome) {
        e.pizza.addAll(ordine.pizza);
        found = true;
      }
    });
    if (!found) prenotazione.lista.add(ordine);
    notifyListeners();
  }

  void addPizzaOnline(Pizza pizza) async {
    pizze.add(pizza);
    Provider.of<FirebaseNotifier>(context).addPizza(pizza);
    notifyListeners();
  }

  void addPizza(Pizza pizza) {
    pizze.add(pizza);
    notifyListeners();
  }
}
