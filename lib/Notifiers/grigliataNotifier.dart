import 'package:casettapp/Models/cibo.dart';
import 'package:casettapp/Models/grigliata.dart';
import 'package:casettapp/Models/invitato.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'firebaseNotifier.dart';

class GrigliataNotifier with ChangeNotifier {
  Grigliata grigliata;

  BuildContext context;
  GrigliataNotifier({this.context});

  void setPersonaAccepted(int personaIndex, bool accettato) {
    grigliata.invitati[personaIndex].accettato = accettato;
    Provider.of<FirebaseNotifier>(context).setPersonaAccettato(
        grigliata.invitati[personaIndex].path,
        grigliata.invitati[personaIndex]);
    notifyListeners();
  }

  void setCiboPreso(int ciboIndex, bool preso) {
    grigliata.cibo[ciboIndex].preso = preso;
    Provider.of<FirebaseNotifier>(context).setCiboPreso(
        grigliata.cibo[ciboIndex].path, grigliata.cibo[ciboIndex]);
    notifyListeners();
  }

  void newGrigliata(Grigliata nGrigliata, String username) {
    nGrigliata.cibo = [];
    nGrigliata.invitati = [
      Invitato(accettato: true, nome: username),
    ];
    grigliata = nGrigliata;
    Provider.of<FirebaseNotifier>(context).newGrigliata(nGrigliata).then((e) {
      nGrigliata.path = e;
    });
    notifyListeners();
  }

//Da utilizzare solamente durante il download della grigliata durante il login --> causa nessuna interazione con il server
  void addGrl(Grigliata ngrigliata) {
    grigliata = ngrigliata;
    notifyListeners();
  }

  void delGrigliata() {
    Provider.of<FirebaseNotifier>(context).delDocument(grigliata.path);
    grigliata = null;
    notifyListeners();
  }

  void delInvitato(int index) {
    Provider.of<FirebaseNotifier>(context)
        .delDocument(grigliata.invitati[index].path);
    grigliata.invitati.removeAt(index);
    notifyListeners();
  }

  void addInvitato(Invitato invitato) async {
    invitato.accettato = false;
    invitato.path = await Provider.of<FirebaseNotifier>(context)
        .addInvitato(invitato, grigliata.path);
    grigliata.invitati.add(invitato);
    notifyListeners();
  }

  void delCibo(int index) {
    Provider.of<FirebaseNotifier>(context)
        .delDocument(grigliata.cibo[index].path);
    grigliata.cibo.removeAt(index);
    notifyListeners();
  }

  void addCibo(Cibo cibo) async {
    cibo.path = await Provider.of<FirebaseNotifier>(context).addCibo(cibo);
    grigliata.cibo.add(cibo);
    notifyListeners();
  }
}
