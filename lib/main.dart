import 'package:casettapp/Models/myBehavior.dart';

import 'package:casettapp/Notifiers/grigliataNotifier.dart';
import 'package:casettapp/Notifiers/mainNotifier.dart';
import 'package:casettapp/Notifiers/pageNotifier.dart';
import 'package:casettapp/Notifiers/pizzeNotifier.dart';

import 'package:casettapp/Pages/Login/login.dart';
import 'package:casettapp/Pages/SkeletonPage/skeletonpage.dart';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

import 'package:provider/provider.dart';

import 'Notifiers/firebaseNotifier.dart';

void main() => runApp(
      ChangeNotifierProvider(
        builder: (context) => FirebaseNotifier(),
        child: ChangeNotifierProvider(
          builder: (context) => GrigliataNotifier(context: context),
          child: ChangeNotifierProvider(
            builder: (context) => PizzeNotifier(context: context),
            child: ChangeNotifierProvider(
              builder: (context) => PageNotifier(),
              child: ChangeNotifierProvider(
                builder: (context) => MainNotifier(context: context),
                child: MyApp(),
              ),
            ),
          ),
        ),
      ),
    );

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    setStatusBarColor();
    return MaterialApp(
      builder: (context, child) {
        return ScrollConfiguration(
          child: child,
          behavior: MyBehavior(),
        );
      },
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [const Locale('it')],
      title: 'CasettApp',
      theme: ThemeData(
          fontFamily: 'GoogleSans',
          primarySwatch: Colors.blue,
          floatingActionButtonTheme: FloatingActionButtonThemeData(
            backgroundColor: Colors.white,
            foregroundColor: Colors.blue[800],
          ),
          appBarTheme: AppBarTheme(
              color: Colors.transparent,
              elevation: 0.0,
              iconTheme: IconThemeData(color: Colors.black),
              textTheme: TextTheme(
                  title: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 20.0)))),
      home: !Provider.of<MainNotifier>(context).loggedIn
          ? Login()
          : SkeletonPage(),
    );
  }

  Future<void> setStatusBarColor() async {
    await FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    await FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
  }
}
